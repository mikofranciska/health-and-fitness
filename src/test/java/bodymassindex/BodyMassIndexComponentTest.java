package bodymassindex;

import bodymassindex.enums.HeightUnit;
import bodymassindex.enums.WeightUnit;
import bodymassindex.models.BodyMassIndexData;
import bodymassindex.models.HeightData;
import bodymassindex.models.PersonalBodyData;
import bodymassindex.models.WeightData;
import org.junit.Test;

public class BodyMassIndexComponentTest {
    @Test
    public void testCalculateWhenHeightAndWeightArePositiveAndUnitsMeterAndKilogram() throws Exception {
        PersonalBodyData personalBodyData = new PersonalBodyData(
                new HeightData(HeightUnit.METER, 2),
                new WeightData(WeightUnit.KILOGRAM, 80));
        BodyMassIndexData expected = new BodyMassIndexData(20, "Normal");
        BodyMassIndexData bodyMassIndexData = BodyMassIndexComponent.calculate(personalBodyData);

        assert(expected.equals(bodyMassIndexData));
    }

    @Test
    public void testCalculateWhenHeightAndWeightArePositiveAndUnitsInchAndPound() throws Exception {
        PersonalBodyData personalBodyData = new PersonalBodyData(
                new HeightData(HeightUnit.INCH, 70),
                new WeightData(WeightUnit.POUND, 180));
        BodyMassIndexData expected = new BodyMassIndexData(25.83, "Overweight");
        BodyMassIndexData bodyMassIndexData = BodyMassIndexComponent.calculate(personalBodyData);

        assert(expected.equals(bodyMassIndexData));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalculateWhenHeightIsZero() {
        PersonalBodyData personalBodyData = new PersonalBodyData(
                new HeightData(HeightUnit.METER, 0),
                new WeightData(WeightUnit.KILOGRAM, 80));

        BodyMassIndexComponent.calculate(personalBodyData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalculateWhenHeightIsNegative() {
        PersonalBodyData personalBodyData = new PersonalBodyData(
                new HeightData(HeightUnit.METER, -2),
                new WeightData(WeightUnit.KILOGRAM, 80));

        BodyMassIndexComponent.calculate(personalBodyData);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalculateWhenHeightDataIsNull() {
        PersonalBodyData personalBodyData = new PersonalBodyData(
                null,
                new WeightData(WeightUnit.KILOGRAM, 0));

        BodyMassIndexComponent.calculate(personalBodyData);
    }

}