package bodymassindex.models;

/**
 * This abstract class represent measured data.
 */
public abstract class MeasuredData {
    private Enum unit;
    private double value;

    /**
     * Convert value to Metric Unit.
     *
     * @return Converted value
     */
    public abstract double convertToMetricUnit();
}
