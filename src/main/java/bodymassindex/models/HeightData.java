package bodymassindex.models;

import bodymassindex.enums.HeightUnit;

/**
 * This class represent height data.
 */
public class HeightData extends MeasuredData {
    private HeightUnit unit;
    private double value;

    public HeightData(HeightUnit unit, double value) {
        this.unit = unit;
        this.value = value;
    }

    public HeightUnit getUnit() {
        return unit;
    }

    public void setUnit(HeightUnit unit) {
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    /**
     * Convert height value to meter.
     *
     * @return Converted value
     */
    public double convertToMetricUnit() {
        return this.value * this.unit.getHeightUnitToMeter();
    }
}
