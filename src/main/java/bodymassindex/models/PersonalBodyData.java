package bodymassindex.models;

/**
 * This class represent the personal body data.
 */
public class PersonalBodyData {

    private HeightData heightData;
    private WeightData weightData;

    public PersonalBodyData(HeightData heightData, WeightData weightData) {
        this.heightData = heightData;
        this.weightData = weightData;
    }

    public HeightData getHeightData() {
        return heightData;
    }

    public void setHeightData(HeightData heightData) {
        this.heightData = heightData;
    }

    public WeightData getWeightData() {
        return weightData;
    }

    public void setWeightData(WeightData weightData) {
        this.weightData = weightData;
    }
}
