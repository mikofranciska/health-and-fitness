package bodymassindex.models;

import java.text.DecimalFormat;

/**
 * This class represent the Body Mass Index data of person.
 */
public class BodyMassIndexData {

    private double bodyMassIndex;
    private String categoryOfBMI;

    public BodyMassIndexData(double bodyMassIndex, String categoryOfBMI) {
        this.bodyMassIndex = bodyMassIndex;
        this.categoryOfBMI = categoryOfBMI;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) return true;
        if (!(object instanceof BodyMassIndexData)) {
            return false;
        }

        final BodyMassIndexData other = (BodyMassIndexData) object;

        return Math.abs(other.bodyMassIndex - this.bodyMassIndex) < 0.01 &&
                other.categoryOfBMI.equals(this.categoryOfBMI);
    }

    @Override
    public String toString() {
        return "Based on the information provided" + "\n" +
                "your Body Mass Index is: " +
                new DecimalFormat("##.##").format(bodyMassIndex) + "\n" +
                "and category of your body weight is: " + categoryOfBMI;
    }
}
