package bodymassindex.models;

import bodymassindex.enums.WeightUnit;

/**
 * This class represent weight data.
 */
public class WeightData extends MeasuredData {
    private WeightUnit unit;
    private double value;

    public WeightData(WeightUnit unit, double value) {
        this.unit = unit;
        this.value = value;
    }

    public WeightUnit getUnit() {
        return unit;
    }

    public void setUnit(WeightUnit unit) {
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    /**
     * Convert weight value to kilogram.
     *
     * @return Converted value
     */
    public double convertToMetricUnit() {
        return this.value * this.unit.getWeightUnitToKilogram();
    }
}
