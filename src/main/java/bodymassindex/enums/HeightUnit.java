package bodymassindex.enums;

/**
 * Represent height units.
 */
public enum HeightUnit {
    METER(1),
    CENTIMETER(0.01),
    INCH(0.0254);

    private final double heightUnitToMeter;

    HeightUnit(double heightUnitToMeter) {
        this.heightUnitToMeter = heightUnitToMeter;
    }

    public double getHeightUnitToMeter() {
        return heightUnitToMeter;
    }
}
