package bodymassindex.enums;

/**
 * Represent weight units.
 */
public enum WeightUnit {
    KILOGRAM(1),
    GRAM(0.001),
    POUND(0.453592);

    private final double weightUnitToKilogram;

    WeightUnit(double weightUnitToKilogram) {
        this.weightUnitToKilogram = weightUnitToKilogram;
    }

    public double getWeightUnitToKilogram() {
        return weightUnitToKilogram;
    }
}
