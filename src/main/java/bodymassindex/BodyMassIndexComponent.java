package bodymassindex;

import bodymassindex.enums.HeightUnit;
import bodymassindex.models.BodyMassIndexData;
import bodymassindex.models.PersonalBodyData;

/**
 * This component calculate the Body Mass Index.
 */
public class BodyMassIndexComponent {

    /**
     * Calculate the Body Mass Index and the category of body weight.
     *
     * @param personalBodyData Instance of PersonalBodyData
     * @return Instance of BodyMassIndexData
     * @throws IllegalArgumentException
     */
    public static BodyMassIndexData calculate(PersonalBodyData personalBodyData) throws IllegalArgumentException {

        BodyMassIndexData bodyMassIndexData;

        if (personalBodyData == null) {
            throw new IllegalArgumentException("PersonalBodyData can not be null!");
        } else if (personalBodyData.getHeightData() == null || personalBodyData.getWeightData() == null) {
            throw new IllegalArgumentException("Measured data (height or data) can not be null!");
        } else if (personalBodyData.getHeightData().getUnit() == null || personalBodyData.getWeightData().getUnit() == null) {
            throw new IllegalArgumentException("Unit of height or weight can not be null!");
        } else if (personalBodyData.getHeightData().getValue() <= 0 || personalBodyData.getWeightData().getValue() <= 0) {
            throw new IllegalArgumentException("Value of height or weight can not be less than zero!");
        } else {
            double bodyMassIndex = calculateBMI(personalBodyData);
            bodyMassIndexData = new BodyMassIndexData(bodyMassIndex, getBMICategory(bodyMassIndex));
        }

        return bodyMassIndexData;
    }

    /**
     * Calculate the Body Mass Index in 'kilogram / (meter * meter)' unit.
     *
     * @return Calculated value
     */
    private static double calculateBMI(PersonalBodyData personalBodyData) {
        double convertedHeight = personalBodyData.getHeightData().convertToMetricUnit();
        double convertedWeight = personalBodyData.getWeightData().convertToMetricUnit();

        return convertedWeight / (convertedHeight * convertedHeight);
    }

    /**
     * Get category of body weight.
     *
     * @param bodyMassIndex Value of Body Mass Index
     * @return Category of body weight
     */
    private static String getBMICategory(double bodyMassIndex) {
        String category;

        if (bodyMassIndex < 16) {
            category = "Severe Thinness";
        } else if (16 <= bodyMassIndex && bodyMassIndex < 17) {
            category = "Moderate Thinness";
        } else if (17 <= bodyMassIndex && bodyMassIndex < 18.5) {
            category = "Mild Thinness";
        } else if (18.5 <= bodyMassIndex && bodyMassIndex < 25) {
            category = "Normal";
        } else if (25 <= bodyMassIndex && bodyMassIndex < 30) {
            category = "Overweight";
        } else if (30 <= bodyMassIndex && bodyMassIndex < 35) {
            category = "Obese Class I";
        } else if (35 <= bodyMassIndex && bodyMassIndex < 40) {
            category = "Obese Class II";
        } else {
            category = "Obese Class III";
        }

        return category;
    }
}
