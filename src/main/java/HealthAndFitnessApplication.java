import bodymassindex.BodyMassIndexComponent;
import bodymassindex.enums.HeightUnit;
import bodymassindex.enums.WeightUnit;
import bodymassindex.models.BodyMassIndexData;
import bodymassindex.models.HeightData;
import bodymassindex.models.PersonalBodyData;
import bodymassindex.models.WeightData;

/**
 * This is a simple health & fitness application.
 *
 * @author Franciska Miko
 * @version 1.0
 * @since 2017-07-13
 */
public class HealthAndFitnessApplication {

    public static void main(String[] args) {

        BodyMassIndexData bodyMassIndexData;

        try {
            bodyMassIndexData = BodyMassIndexComponent.calculate(
                    new PersonalBodyData(
                            new HeightData(HeightUnit.METER, 1.63),
                            new WeightData(WeightUnit.KILOGRAM, 60)));

            System.out.println(bodyMassIndexData.toString());
        } catch (IllegalArgumentException ex) {
            System.out.println(ex.toString());
        }
    }
}